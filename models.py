from database import db


class PensionFundSerialModel(db.Model):
    """ Модель, отражающая базу данных СНИЛС. """
    __tablename__: str = 'serials'
    __table_args__: dict[str, bool] = {'quote': False}
    id = db.Column(db.Integer, primary_key=True)
    serial = db.Column(db.String, unique=True, nullable=False)

    def __repr__(self) -> str:
        return f'{self.serial}'
