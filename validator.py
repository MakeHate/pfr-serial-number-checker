from models import PensionFundSerialModel
from time import gmtime, strftime
from io import TextIOWrapper
from create_app import app
from settings import mySettings


def correctSerialChecker(serialCorrect: str) -> bool:
    """ Данная функция проверяет корректность СНИЛС путем проверки контольной суммы. """
    serialCorrectResult: bool = False
    serailCorrectNoSpaces: str = serialCorrect.replace(' ', '')
    calculatedControlValue: int = checkingControlValue(serailCorrectNoSpaces)
    if isinstance(calculatedControlValue, int):
        checkingNumValidValue, checkingNumTestValue = getComparisonSerialValues(
            calculatedControlValue, serailCorrectNoSpaces
        )
        if checkingNumValidValue == checkingNumTestValue:
            serialCorrectResult: bool = True
    return serialCorrectResult


def findInDatabase(serialExistsDB: str) -> bool:
    """ Данная функция проверяет наличие СНИЛС, введенного пользователем в базе данных. """
    serialFoundResult: bool = False
    foundSerial: str = str(PensionFundSerialModel.query.filter_by(
        serial=serialExistsDB
    ).all()).strip('[]')
    if serialExistsDB == foundSerial:
        serialFoundResult: bool = True
    return serialFoundResult


def checkingControlValue(serialExistsDB: str) -> int:
    """ Данная функция возвращает сумму всех элементов СНИЛС, умноженных на значения от 1 до 9. """
    total: int = 0
    index: int = 0
    values: tuple[
        int, int, int,
        int, int, int,
        int, int, int
        ] = (
            9, 8, 7,
            6, 5, 4,
            3, 2, 1
        )
    while index != len(values):
        total += int(serialExistsDB[index]) * values[index]
        index += 1
    return total


def getComparisonSerialValues(calculatedControlValue: int,
                              serialExistsDB: str) -> tuple[int, int]:
    """ Данная функция возвращает правильную контольную сумму СНИЛС и контрольную сумму СНИЛС пользователя. """
    CHECK_NUMBER: int = 101
    CONTROL_VALUE: int = -2
    checkingNumValidValue: int = calculatedControlValue % CHECK_NUMBER
    checkingNumTestValue: int = int(serialExistsDB[CONTROL_VALUE:])

    return checkingNumValidValue, checkingNumTestValue


def validateForm(serialName: str) -> tuple[str, str]:
    """ Данная функция проверяет СНИЛС, полученный от пользователя. """
    serialCorrect: str = ''
    serialExistsDB: str = ''
    if correctSerialChecker(serialName):
        serialCorrect = f'Контольная сумма {serialName} действительна.'
    else:
        serialCorrect = f'Контольная сумма {serialName} недействительна.'
    if findInDatabase(serialName):
        serialExistsDB = f'СНИЛС {serialName} обнаружен в БД.'
    else:
        serialExistsDB = f'СНИЛС {serialName} не обнаружен в БД.'
    return serialExistsDB, serialCorrect


def validateFile(filename: str) -> str:
    """ Данная функция проверяет СНИЛС в файле, полученном от пользователя. """
    getCurrentTime: str = strftime("%Y-%m-%d %H:%M:%S", gmtime())
    tempName: str = f'{getCurrentTime}.txt'
    serialCorrect: str = ''
    serialExistsDB: str = ''
    try:
        tempFile: TextIOWrapper = open(rf'{mySettings.UPLOAD_FOLDER}/{tempName}',
                                       'w',
                                       encoding='utf-8')
        with open(rf'uploads/{filename}', 'r') as serialsFile:
            for line in serialsFile:
                correctLine: str = line.replace('-', ' ').strip()
                fromIsDigitLine: str = correctLine.replace(' ', '').rstrip('\n')
                if fromIsDigitLine.isdigit() and len(correctLine) <= 14:
                    serialExistsDB, serialCorrect = validateForm(correctLine)
                    tempFile.write(
                        f'{serialExistsDB} {serialCorrect} \n'
                    )
                else:
                    tempFile.write(
                        f'{line}, [ОШИБКА] Содержит буквы ИЛИ неверное число символов \n'
                    )
            tempFile.close()
    except FileNotFoundError as err:
        app.logger.debug(f'Файл не обнаружен: {err}')
    except Exception as err:
        app.logger.debug(f'Произошла ошибка: {err}')
    return tempName
