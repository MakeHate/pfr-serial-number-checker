class Settings:
    """ Класс, хранящий в себе настройки проекта. """
    def __init__(self) -> None:
        self.UPLOAD_FOLDER: str = r'uploads'
        self.ALLOWED_EXTENSIONS: set[str] = set(['txt'])
        
mySettings: Settings = Settings()