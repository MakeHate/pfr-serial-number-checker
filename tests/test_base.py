from flask import Flask, current_app
from flask.ctx import AppContext
from create_app import create_app
from database import db
import unittest
import sys
sys.path.append(
    r"/home/makehate/Программирование/[Open] PensionFundRefactoring/"
)


class BaseTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.app: Flask = create_app()
        self.app.config.update({
            'SQLALCHEMY_DATABASE_URI': 'sqlite:///pfr_test.db',
            'SQLALCHEMY_TRACK_MODIFICATIONS': False,
            'TESTING': True
        })
        self.app_context: AppContext = self.app.app_context()
        self.app_context.push()
        db.init_app(self.app)
        db.create_all()

    def tearDown(self) -> None:
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_app_exists(self) -> None:
        self.assertFalse(current_app is None)

    def test_app_is_testing(self) -> None:
        self.assertTrue(current_app.config['TESTING'])


if __name__ == '__main__':
    unittest.main()
