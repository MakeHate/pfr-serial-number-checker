from test_base import BaseTestCase
from database import db
from models import PensionFundSerialModel
import unittest
import sys
sys.path.append(
    r"/home/makehate/Программирование/[Open] PensionFundRefactoring/"
)


class ModelsTestCase(BaseTestCase):
    def __init__(self, *args, **kwargs) -> None:
        super(ModelsTestCase, self).__init__(*args, **kwargs)
        self.serial: str = '495 412 126 92'

    def test_serial_get_correct(self) -> None:
        postSerial: PensionFundSerialModel = PensionFundSerialModel(serial=self.serial)
        db.session.add(postSerial)
        getSerial = postSerial.query.filter_by(serial=self.serial).scalar()
        self.assertEqual(postSerial, getSerial)


if __name__ == '__main__':
    unittest.main()
