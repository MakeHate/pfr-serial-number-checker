from settings import mySettings
from validator import validateForm, validateFile
from werkzeug.utils import secure_filename
from werkzeug.datastructures import ImmutableMultiDict
from flask.wrappers import Request
from os import mkdir
from os.path import join, exists


def typeForm(request: Request, serialExistsDB: str,
             serialCorrect: str) -> tuple[str, str]:
    """ Данная функция отвечает за порядок действий, если данные из HTML-формы. """
    requestForm: ImmutableMultiDict = request.form
    serialFromForm: str = str(requestForm.get('serialForm'))
    serialExistsDB, serialCorrect = validateForm(serialFromForm)
    return serialExistsDB, serialCorrect


def typeFile(request: Request) -> str:
    """ Данная функция отвечает за порядок действий, если пользователь отправил файл. """
    if not exists(mySettings.UPLOAD_FOLDER):
        mkdir(f"{mySettings.UPLOAD_FOLDER}")
    fileForm: ImmutableMultiDict = request.files
    file = fileForm['file']
    if file and allowedFile(file.filename):
        filename = secure_filename(file.filename)
        file.save(join(mySettings.UPLOAD_FOLDER, filename))
        serialsFileName: str = validateFile(filename)
    return serialsFileName


def allowedFile(filename: str) -> bool:
    """ Вспомогательная функция для проверки расширения файла. """
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in mySettings.ALLOWED_EXTENSIONS
