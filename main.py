"""
Веб-приложение разработано на основе тестового задания, выданного Пенсионным Фондом Российской Федерации на позицию программиста баз данных.
Веб приложение должно давать пользователю возможность вводить СНИЛС, а так же отправлять файл, содержащий СНИЛС и проверять их на корректность,
используя алгоритм, опубликованный на сайте Пенсионного Фонда Российской Федерации. Так же приложение должно проверять, есть ли такой СНИЛС в базе
данных в принципе и если он отсутствует или некорректен, возвращать ответ. Задание не предполагает добавления СНИЛС, введенного пользоваталем в базу
данных. В задании отсутсвует информация о том, как расфасованы СНИЛС в файле, отправленном пользователем. Больше деталей о задании можно найти на GitLab.

@ Pavel Tereschuck, 2022 год;
@ GitLab: https://gitlab.com/MakeHate;
@ Mail: solarium228@gmail.com;
@ Discord: @MakeHate#6753.
"""

from flask import render_template, request, send_from_directory
from flask.wrappers import Response
from settings import mySettings
from database import db
from selector import typeForm, typeFile
from create_app import app
from os import listdir, remove
from os.path import join
from logging import basicConfig, DEBUG


@app.route('/', methods=['POST', 'GET'])
def index() -> Response | str:
    """ Функция представления, в которой реализована основная логика. """
    fileName: str = ''
    serialCorrect: str = ''
    serialExistsDB: str = ''
    if request.method == 'POST':
        if request.form:
            serialExistsDB, serialCorrect = typeForm(request,
                                                     serialExistsDB,
                                                     serialCorrect)
        if request.files:
            fileName = typeFile(request)
            return send_from_directory(mySettings.UPLOAD_FOLDER, fileName)
    else:
        directory: str = f'{mySettings.UPLOAD_FOLDER}/'
        for file in listdir(directory):
            remove(join(directory, file))
        else:
            pass
    return render_template('index.html',
                           title='Поиск в БД и проверка корректности СНИЛС',
                           information='Введите СНИЛС для поиска и проверки',
                           checkingDatabaseSerial=serialExistsDB,
                           checkingCorrectnessSerial=serialCorrect)


if __name__ == '__main__':
    formated = f'%(asctime)s %(levelname)s %(name)s : %(message)s'
    basicConfig(filename='logs/server.log',
                level=DEBUG,
                format=formated)
    db.init_app(app)
    with app.app_context():
        db.create_all()
    app.run(debug=True)
