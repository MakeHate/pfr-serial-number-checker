from flask import Flask
from settings import mySettings


def create_app() -> Flask:
    app: Flask = Flask(__name__)
    app.config['UPLOAD_FOLDER'] = mySettings.UPLOAD_FOLDER
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///pfr.db'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['TESTING'] = False
    return app


app: Flask = create_app()
